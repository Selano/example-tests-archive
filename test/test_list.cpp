#include "../include/classA.hpp"
#include <gtest/gtest.h>
#include <string>

TEST(classA, intTest) {
    // Arrange
    std::string msgNo("No");
    std::string msgOk("Ok");
    A obj; 

    // Act

    // Assert
    ASSERT_EQ(obj.retint(msgNo), 0);
    ASSERT_EQ(obj.retint(msgOk), 0);
}

TEST(classA, strTest) {
    // Arrange
    int a = 0, b = 1;
    A obj; 

    // Act

    // Assert
    ASSERT_EQ(obj.retstr(b), "OK");
    ASSERT_EQ(obj.retstr(a), "Error");
}