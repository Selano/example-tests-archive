#pragma once

#include <string>

class A {
    private:
        A(const A&) = delete;
        A& operator=(const A&) = delete;

    public:
        A();
        std::string retstr(int code);
        int retint(std::string code);
};