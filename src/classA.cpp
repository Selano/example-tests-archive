#include "../include/classA.hpp"

A::A() {

}

std::string A::retstr(int code) {
    if(code > 0) 
        return "OK";
    else
        return "Error";
}

int A::retint(std::string code) {
    if(code == "OK") 
        return 1;
    else
        return 0;
}